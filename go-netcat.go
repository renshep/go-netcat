package main

import (
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"strings"
	"sync"
)

type WriterMap map[io.Writer]bool
type ChanMap map[chan []byte]bool

var CmdStartWait sync.WaitGroup

func ReaderToChan(ior io.Reader, cm ChanMap, errf *func()) {
	CmdStartWait.Done()
	buf := make([]byte, 1024)
	for {
		count, err := ior.Read(buf)
		if err != nil {
			if errf != nil {
				(*errf)()
			}
			break
		}
		for c, ca := range cm {
			if ca {
				select {
				case c <- buf[:count]: // non-blocking send, drop data if buffers are full
					continue
				default:
					continue
				}
			}
		}
	}
}

func ChanToWriters(p chan []byte, wm WriterMap, lock *sync.Mutex) {
	CmdStartWait.Done()
	for data := range p {
		if lock != nil {
			lock.Lock()
		}
		for w, wa := range wm {
			if wa {
				_, err := w.Write(data)
				if err != nil {
					wm[w] = false
				}
			}
		}
		if lock != nil {
			lock.Unlock()
		}
	}
}

func main() {
	// exit signal channel
	ExitMain := make(chan bool)

	// logging setup
	logr := log.New(os.Stderr, os.Args[0]+` - `, log.Ldate|log.Ltime|log.Lmicroseconds|log.Llongfile) // TODO: REMOVE THIS LINE AND USE THE BELOW INSTEAD
	//logr := log.New(os.Stderr, "", log.Ldate|log.Ltime)
	debugprint := logr.Println // TODO: REMOVE ALL debugprint* lines
	defer debugprint(`main exit`)

	// error handling
	exit := logr.Fatalln
	e := func(aerr interface{}) {
		if aerr != nil {
			exit(aerr)
		}
	}

	// argument parsing
	if len(os.Args) < 4 {
		exit(`Usage:`, os.Args[0], `d[ial]|l[isten] tcp|tcp4|tcp6|unix|unixpacket address [cmd [args...]]`)
	}
	stype := string(strings.ToLower(os.Args[1])[0])
	sproto := strings.ToLower(os.Args[2])
	saddr := os.Args[3]
	debugprint(`stype:`, stype, `sproto:`, sproto, `saddr:`, saddr)
	usecmd := false
	var scmd []string
	if len(os.Args) > 4 {
		scmd = os.Args[4:]
		usecmd = true
	}
	debugprint(`usecmd:`, usecmd, `scmd:`, scmd)

	// IO SETUP
	var (
		REMOTEIN  = make(chan []byte, 1024)
		REMOTEOUT = make(chan []byte, 1024)
		CMDIN     = make(chan []byte, 1024)
		CMDOUT    = make(chan []byte, 1024)
	)

	NullExitFunc := func() {}
	SimpleExitFunc := func() {
		os.Exit(0)
	}
	StdInExitFunc := &SimpleExitFunc

	RemoteInWriterMap := WriterMap{
		os.Stdout: true,
	}
	StdInChanMap := ChanMap{
		REMOTEOUT: true,
	}
	CmdStartWait.Add(1) // one extra waiter for the dialer or listener to signal
	if usecmd {
		cmd := exec.Command(scmd[0], scmd[1:]...)
		cmdin, err := cmd.StdinPipe()
		e(err)
		cmdout, err := cmd.StdoutPipe()
		e(err)
		cmd.Stderr = os.Stderr
		StdInChanMap[CMDIN] = true      // add CMDIN channel to os.Stdin pipe
		RemoteInWriterMap[cmdin] = true // add child command stdin to the list of things that REMOTEIN are written to
		CmdOutChanMap := ChanMap{
			REMOTEOUT: true, // route child command stdout from the command to the remote server
			CMDOUT:    true, // and also to CMDOUT
		}
		CommandOutWriterMap := WriterMap{
			os.Stdout: true, // also write child command stdout to our stdout
		}
		StdInExitFunc = &NullExitFunc // in command mode, don't exit when stdin reaches EOF or errors out
		// start all the readers and writers
		CmdStartWait.Add(3)
		go func() {
			debugprint(`cmdoutput reader started`)
			ReaderToChan(cmdout, CmdOutChanMap, StdInExitFunc)
			debugprint(`cmdoutput reader ended`)
		}()
		go func() {
			debugprint(`cmdoutput writer started`)
			ChanToWriters(CMDOUT, CommandOutWriterMap, nil)
			debugprint(`cmdoutput writer ended`)
		}()
		go func() {
			debugprint(`cmdin writer started`)
			ChanToWriters(CMDIN, WriterMap{cmdin: true}, nil)
			debugprint(`cmdin writer started`)
		}()
		// start the child command
		go func() {
			CmdStartWait.Wait() // but wait for the signal
			debugprint(`command started`, scmd)
			cmd.Run()
			debugprint(`command exited`, scmd)
			SimpleExitFunc() // exit when the child command exits
		}()
	}
	// start the main readers and writers
	CmdStartWait.Add(2)
	go func() {
		debugprint(`stdin reader started`)
		ReaderToChan(os.Stdin, StdInChanMap, StdInExitFunc)
		debugprint(`stdin reader ended`)
	}()
	go func() {
		debugprint(`remote in writer started`)
		ChanToWriters(REMOTEIN, RemoteInWriterMap, nil)
		debugprint(`remote in writer ended`)
	}()

	// wait for command to start
	if stype == "d" {
		// dialer
		debugprint(`starting dialer...`, sproto, saddr)
		rcon, err := net.Dial(sproto, saddr)
		e(err)
		defer rcon.Close()
		debugprint(`connected to`, rcon.RemoteAddr())
		ConWriters := WriterMap{
			rcon: true, // pipe the REMOTEOUT channel to the remote connection
		}
		ConReaders := ChanMap{
			REMOTEIN: true, // pipe input from the remote connection to the REMOTEIN channel
		}
		// start the readers and writers for the remote connection
		CmdStartWait.Add(2)
		go ChanToWriters(REMOTEOUT, ConWriters, nil)
		go ReaderToChan(rcon, ConReaders, StdInExitFunc)
		CmdStartWait.Done() // start child command
	} else if stype == "l" {
		// listener
		l, err := net.Listen(sproto, saddr)
		e(err)
		defer l.Close()
		ConWriters := WriterMap{} // pipe the REMOTEOUT channel to ALL the remote connections
		var ConWritersSync sync.Mutex
		debugprint(`listening on`, sproto, saddr)
		// start the remote writer for all connections
		CmdStartWait.Add(1)
		go ChanToWriters(REMOTEOUT, ConWriters, &ConWritersSync)
		CmdStartWait.Done() // signal child command to start
		// accept new connections in a loop
		for {
			con, err := l.Accept()
			debugprint(err)
			go func(rcon net.Conn) {
				debugprint(`connected to`, rcon.RemoteAddr())
				ConWritersSync.Lock()
				ConWriters[rcon] = true // add connection to list
				ConWritersSync.Unlock()
				RemoteReaderExitFunc := func() {
					debugprint(`end of input from`, rcon.RemoteAddr())
					ConWritersSync.Lock()
					delete(ConWriters, rcon)
					ConWritersSync.Unlock()
					rcon.Close()
				}
				debugprint(rcon.RemoteAddr(), `exit function is`, RemoteReaderExitFunc, &RemoteReaderExitFunc)
				ConReaders := ChanMap{
					REMOTEIN: true, // pipe input from the remote connection to the REMOTEIN channel
				}
				CmdStartWait.Add(1)
				go ReaderToChan(rcon, ConReaders, &RemoteReaderExitFunc)
			}(con)
		}
	} else {
		exit("ERROR: you must pick d[ial] or l[isten]")
	}

	// wait for exit signal
	debugprint(`waiting for exit signal`)
	<-ExitMain

}
